# This script depends on Pystitch module
from pystrich.datamatrix import DataMatrixEncoder

message = 'This is a DataMatrix.\n Name: Neil Degrasse Tyson \n Show: Cosmos'
encoder = DataMatrixEncoder(message)
encoder.save('./datamatrix_test.png')
print(encoder.get_ascii())
